# Przygoda małżeńska app

## Description

Simple web application used offline. For given code-key pairs an application verifies code aquired by a person.

This application just supports game called "Przygoda małżeńska[^1]", it has some puzzles to solve and is played outside.

## Usage

npm start

It literally runs http-server on the folder.

## What it supports

* main feature kinda works
* codes and keys are read from .csv file (it doesn't work under chrome, because CORS policy disallows non-http requests)
* multiple language support (however no language switch is added)

## TODO

* tests (well, I will do I swear xd )

## Sources

[favicon](https://www.freefavicon.com/freefavicons/people/iconinfo/family-152-147937.html)

[^1]: Polish name, direct translation is "Spouses' Adventure", but we still haven't decided on English name.