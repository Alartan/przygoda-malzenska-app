const fs = require("fs");

module.exports = function checkCSV(
  file = "/../kody.csv",
  code_length = 20,
  key_length = 4
) {
  var CSV_lines = fs.readFileSync(__dirname + file, "UTF-8").split("\n");
  if ("code, key" == CSV_lines.shift()) {
    console.log("\tFirst line of CSV is OK");
  } else {
    console.log("\n!BAD! First line of CSV!\n");
    return false;
  }
  var result = CSV_lines.map(single_line =>
    validateLine(single_line.split(", "), code_length, key_length)
  ).reduce((x, y) => x && y, true);
  if (result == true) {
    console.log("\tCodes and keys in CSV are OK");
  }
  return result;
};

function validateLine(pair, code_length, key_length) {
  if (pair[0].length != code_length) {
    console.log("\n!BAD! Something wrong with code in \""+pair+"\"!\n");
    return false;
  }
  if (
    String(pair[1]).length != key_length ||
    Number(String(pair[1])) != pair[1]
  ) {
    console.log("\n!BAD! Something wrong with key in \""+pair+"\"!\n");
    return false;
  }
  return true;
}
